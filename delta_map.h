#pragma once

#include "mapa.h"

class DeltaMap {
    public:
      DeltaMap(const Mapa m);
      Mapa optimizacionPI();

    private:
        Mapa map;
};