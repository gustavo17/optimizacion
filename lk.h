#pragma once
#include "oneTree.h"
#include "solucion.h"
#include <optional>
#include <random>

#define ENTRE(menor, mayor, entre) \
    ((menor->posicion <= entre->posicion && entre->posicion <= mayor->posicion) || \
     ((mayor->posicion < menor->posicion) && ((menor->posicion <= entre->posicion) || (entre->posicion <= mayor->posicion))))

class LinKerninghan {
    public:
        int search(Solucion & inicial,const OneTree &ot,const std::optional<Solucion> &mejor, std::mt19937 &engine);
        void ejecutar(Solucion &inicial, const OneTree &ot,
                      const std::optional<Solucion> &mejor,  std::mt19937 &engine);
};