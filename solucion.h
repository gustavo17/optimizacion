#pragma once
#include <limits>
#include <ostream>
#include <vector>
#include "mapa.h"
#include "nodo.h"
#include "oneTree.h"

enum class Estado {
  Factible,
  NoFactible
};

class Solucion {
    public:
      //Construye solucion aleatoria
      Solucion(int n, const OneTree & ot);
      Solucion(std::vector<int> ruta);
      //clona solucion a partir de otra
      Solucion(const Solucion &sol);
      //las dos sgtes mueven soluciones
      Solucion(Solucion &&sol);
      Solucion &operator=(Solucion &&sol);

      void visualizar(std::ostream &out,bool ccw=true) const;
      int evaluar(const Mapa &m);
      bool isConexa() const;

      //atributos principales
      int costo=std::numeric_limits<int>().infinity();
      int getSize() const;
      void move(Nodo *t0, Nodo *t1, Nodo *t2, Nodo *t3);
      void move_esp1(Nodo *t0, Nodo *t1, Nodo *t2, Nodo *t3, Nodo *t4,Nodo *t5);
      void move_esp2(Nodo *t0, Nodo *t1, Nodo *t2, Nodo *t3, Nodo *t4,Nodo *t5);
      void move_noseq(Nodo *t0, Nodo *t1, Nodo *t2, Nodo *t3, Nodo *_t0,
                      Nodo *_t1, Nodo *_t2, Nodo *_t3);
      void move_esp3(Nodo *t0, Nodo *t1, Nodo *t2, Nodo *t3, Nodo *t4, Nodo *t5,
                     Nodo *t6, Nodo *t7);
      void move_esp4(Nodo *t0, Nodo *t1, Nodo *t2, Nodo *t3, Nodo *t4, Nodo *t5,
                     Nodo *t6, Nodo *t7);
      std::vector<Nodo> ady;
      std::uniform_int_distribution<int> seleccionNodo; 
    private:
      void _move(Nodo *t0, Nodo *t1, Nodo *t2, Nodo *t3);
      void _move_esp1(Nodo *t0, Nodo *t1, Nodo *t2, Nodo *t3,Nodo *t4,Nodo*t5);
      void _move_esp2_0(Nodo *t0, Nodo *t1, Nodo *t2, Nodo *t3, Nodo *t4, Nodo *t5);
      void _move_esp2_1(Nodo *t0, Nodo *t1, Nodo *t2, Nodo *t3, Nodo *t4, Nodo *t5);
      void _move_esp2_2(Nodo *t0, Nodo *t1, Nodo *t2, Nodo *t3, Nodo *t4, Nodo *t5);

};