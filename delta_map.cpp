#include "delta_map.h"
#include <algorithm>
#include <limits>
#include <iostream>
#include <numeric>

DeltaMap::DeltaMap(const Mapa m):map{std::move(m)} {
    int i=map.data.size();
}

Mapa DeltaMap::optimizacionPI() {
    std::cout<<"optimizando la funcion beta de mapa"<<std::endl;
    int step=69; //15
    int n=map.data.size();
    int W=std::numeric_limits<int>().min();
    int mejorNorma=std::numeric_limits<int>().max();
    std::vector<int> pi(n,0);
    std::vector<int> mejorPI(n,0);
    int nSinMejora=0;
    while (true){
        if (nSinMejora++==10) { //n/4
            step>>=1;
            nSinMejora=0;
            if (step==0) break;
        }
        std::vector<int> padres(n,-1);
        int largo=map.prim(padres,&pi);

        std::vector<int> vk(n,-1); //porque -2 + el padre -1
        vk[0]=0; // porque la ciudad de origen siempre tendra 2 aristas por construccion
        vk[1]=-2; //porque no tiene padre, depende de donde se conecte
        for (int i=2;i<n;i++){
            vk[padres[i]]++;
        }

        int v1=std::numeric_limits<int>().max();
        int v2=v1;
        int i1=-1;
        int i2=-1;
        for (int i=1;i<n;i++){
            int value = map.data[0][i] + pi[i];
            if (value < v1) {
                v2 = v1;
                i2 = i1;
                v1 = value;
                i1 = i;
            } else if (value < v2) {
                v2 = value;
                i2 = i;
            }            
        }
        vk[i1]++;vk[i2]++; //se conectan las dos menores


        int sumaPi=std::accumulate(pi.begin(),pi.end(),0);

        int w=largo+v1+v2-(2*sumaPi);
        int norma=std::accumulate(vk.begin(),vk.end(),0,[](int sum, int elem){
            return sum + elem * elem;
        });

        if (w>W || (w==W && mejorNorma>norma)){
            //std::cout<<"NORMA="<<norma<<", w="<<w<<std::endl;
            nSinMejora=0;
            W=w;
            mejorNorma=norma;
            std::copy(pi.begin(),pi.end(),mejorPI.begin());
        }

        std::transform(pi.begin(),pi.end(),vk.begin(),pi.begin(),[=](int pi_value,int vk_value){
            return pi_value+step*vk_value;
        });
    }

    for (int i=0;i<n;i++){
        auto &datai=map.data[i];
        for (int j=0;j<n;j++){
            if (i!=j){
                datai[j]=datai[j]+mejorPI[i]+mejorPI[j];
            }
        }
    }

    std::cout<<"optimizacion terminada, W="<<W<<", NORMA="<<std::sqrt(mejorNorma)<<std::endl;
    return map;
}
