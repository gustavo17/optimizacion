
#include <random>
#include "lk.h"
#include "nodo.h"
#include <iostream>

int LinKerninghan::search(Solucion & inicial,const OneTree &ot,const std::optional<Solucion> &mejor, std::mt19937 &engine) {
    int n=inicial.ady.size();
    auto & costo=ot.mapa.data;
    Nodo *t0,*t1;

    Nodo * inicio=&inicial.ady[inicial.seleccionNodo(engine)];
    t0=inicio;
    
    while (true){
        t1=t0->siguiente;
        if (!mejor.has_value()){
            if (ot.getAlfa(t0->id, t1->id)!=0) break;
        } else {
            if (mejor->ady[t0->id].siguiente->id!=t1->id && mejor->ady[t0->id].anterior->id!=t1->id) break;
        }
        t0=t1;
        if (t0==inicio) return 0; // sale al dar la vuelta completa (llegar a inicio de nuevo)
    }

    // //selecciona nodo inicial aleatoriamente, si la solucion inicial contiene arista t0,t1 se busca otro inicio


    int g0=costo[t0->id][t1->id];

    //los selecciona en orden alfa
    for (auto ct2:ot.candidatos[t1->id]){
        Nodo *t2=&inicial.ady[ct2];
        int G0=g0-costo[t1->id][ct2];
        if (G0<=0 || t2==t1->siguiente || t2==t1->anterior)continue;

        //seleccion de X2
        for (int i=0;i<2;i++){
            Nodo *t3=(i==0)?t2->anterior:t2->siguiente;
            
            //i==0 criterio de factibilidad
            //y criterio de ganancia
            int g1=G0+costo[t2->id][t3->id]-costo[t3->id][t0->id];
            if (g1>0){
                if (i==0){
                    inicial.move(t0,t1, t2, t3);
                    return g1;
                } else {
                    Nodo * _t0=t3;
                    while (_t0!=t0) {
                        Nodo * _t1=_t0->siguiente;
                        for (auto cand:ot.candidatos[_t1->id]){
                            Nodo *_t2=&inicial.ady[cand];
                            if (!ENTRE(t1,t2,_t2)) continue;
                            int gan1=g1+costo[_t0->id][_t1->id]-costo[_t1->id][_t2->id];
                            if (gan1<=0) continue;
                            for (int k=0;k<2;k++){
                                Nodo *_t3=(k==0)?_t2->anterior:_t2->siguiente;
                                if (_t3==t0 && _t2==t1) continue;
                                if (_t3==t3 && _t2==t2) continue;
                                int gan2=gan1+costo[_t2->id][_t3->id]-costo[_t3->id][_t0->id];
                                if (gan2<=0) continue;
                                inicial.move_noseq(t0, t1, t2, t3, _t0, _t1, _t2, _t3);          
                                return gan2;
                            }
                        }
                        _t0=_t0->siguiente;
                    }
                }
            } 

            //3-opt
            for (auto ct4:ot.candidatos[t3->id]){
                Nodo *t4=&inicial.ady[ct4];
                int G1=G0+costo[t2->id][t3->id]-costo[t3->id][t4->id];
                if (G1<=0 || t4==t3->siguiente || t4==t3->anterior)continue;

                if (i==0) {
                    bool post4=ENTRE(t1,t3,t4);
                    Nodo* t5=post4?t4->siguiente:t4->anterior;
                    int g2=G1+costo[t4->id][t5->id]-costo[t5->id][t0->id];
                    if (g2>0){
                        inicial.move(t0,t1, t2, t3);
                        inicial.move(t0,t3, t4, t5);
                        return g2;
                    }

                    //4-opt
                    for (auto ct6:ot.candidatos[t5->id]){
                        Nodo *t6=&inicial.ady[ct6];
                        int G2=G1+costo[t4->id][t5->id]-costo[t5->id][t6->id];
                        if (G2<=0 || t6==t5->siguiente || t6==t5->anterior ) continue;
                        Nodo *t7;
                        if (post4){ //t4 entre t1 y t3
                            t7=ENTRE(t1,t4,t6)?t6->siguiente:t6->anterior;
                        } else {
                            t7=ENTRE(t2,t5,t6)?t6->siguiente:t6->anterior;
                        }
                        if ((t7==t0 && t6==t1)||(t7==t3 && t6==t2)) continue;
                        if ((t7==t1 && t6==t0)||(t7==t2 && t6==t3)) continue;
                        int g3=G2+costo[t6->id][t7->id]-costo[t7->id][t0->id];
                        if (g3>0){
                            inicial.move(t0,t1, t2, t3);
                            inicial.move(t0,t3, t4, t5);
                            inicial.move(t0,t5, t6, t7);
                            return g3;
                        }
                        //5-opt
                        for (auto ct8:ot.candidatos[t7->id]){
                            Nodo *t8=&inicial.ady[ct8];
                            int G3=G2+costo[t6->id][t7->id]-costo[t7->id][t8->id];
                            if (G3<=0 || t8==t7->siguiente || t8==t7->anterior ) continue;
                            if ((t7==t1 && t8==t2) || (t7==t3 && t8==t4) ) {
                                continue;
                            }
                            if ((t7==t2 && t8==t1) || (t7==t4 && t8==t3)) {
                                continue;
                            }
                            Nodo *t9;

                            //criterios de seleccion factible de t9
                            if (post4){
                                if (ENTRE(t2,t0,t6)){
                                    if (ENTRE(t6,t0,t8) || ENTRE(t1,t4,t8)){
                                        t9=t8->anterior;
                                    } else {
                                        t9=t8->siguiente;
                                    }
                                } else if  (ENTRE(t1,t4,t6)) {
                                    if (ENTRE(t7,t4,t8) || ENTRE(t2,t0,t8)) {
                                        t9=t8->anterior;
                                    } else {
                                        t9=t8->siguiente;
                                    }
                                } else {
                                    if (ENTRE(t6,t3,t8) || ENTRE(t2,t0,t8)){
                                        t9=t8->anterior;
                                    } else {
                                        t9=t8->siguiente;
                                    }
                                }
                            } else {
                                if (ENTRE(t1,t3,t6)) {
                                    if (ENTRE(t1,t7,t8)){
                                        t9=t8->siguiente;
                                    } else {
                                        t9=t8->anterior;
                                    }
                                } else if (ENTRE(t4,t0,t6)) {
                                    if (ENTRE(t6,t0,t8) || ENTRE(t2,t5,t8)){
                                        t9=t8->anterior;
                                    } else {
                                        t9=t8->siguiente;
                                    }
                                } else {
                                    if (ENTRE(t2,t6,t8)) {
                                        t9=t8->siguiente;
                                    } else {
                                        t9=t8->anterior;
                                    }
                                }
                            }
                            if ((t9==t0 && t8==t1)||(t9==t3 && t8==t2)||(t9==t5 && t8==t4)) continue;
                            if ((t9==t1 && t8==t2)||(t9==t2 && t8==t3)||(t9==t4 && t8==t5)) continue;
                            int g4=G3+costo[t8->id][t9->id]-costo[t9->id][t0->id];
                            if (g4>0){
                                inicial.move(t0,t1, t2, t3);
                                inicial.move(t0,t3, t4, t5);
                                inicial.move(t0,t5, t6, t7);
                                inicial.move(t0,t7, t8, t9);
                                return g4;
                            }
                        }
                    }
                        
                } else { //No Factible
                    //secuencias para factibilizar secuenciales con t4 entre t2 y t0
                    if(ENTRE(t1,t2,t4)){
                        for (int k=0;k<2;k++){
                            if (k==0){
                                Nodo *t5=t4->siguiente;
                                int g2=G1+costo[t4->id][t5->id]-costo[t5->id][t0->id];
                                if (g2>0){
                                    inicial.move_esp1(t0,t1,t2,t3,t4,t5);
                                    return g2;
                                }
                            } else {
                                Nodo *t5=t4->anterior;
                                if (t5==t0 && t4==t1) continue;
                                int g2=G1+costo[t4->id][t5->id]-costo[t5->id][t0->id];
                                if (g2>0){
                                    inicial.move_esp2(t0,t1,t2,t3,t4,t5);
                                    return g2;
                                }
                            }
                        }
                    } else {
                        Nodo * t5=t4->anterior; //obligado
                        for (auto cand:ot.candidatos[t5->id]){
                            Nodo *t6=&inicial.ady[cand];
                            if (!ENTRE(t1,t2,t6)) continue;
                            for (int k=0;k<2;k++){
                                Nodo *t7=(k==0)?t6->siguiente:t6->anterior;
                                if (t6==t2 && t7==t3) continue; //bordes
                                if (t6==t1 && t7==t0) continue; //bordes
                                int g2=G1+costo[t4->id][t5->id]-costo[t5->id][t6->id]
                                            +costo[t6->id][t7->id]-costo[t7->id][t0->id];
                                if (g2>0){
                                    if (k==0) {
                                        inicial.move_esp3(t0, t1, t2, t3, t4, t5, t6, t7);
                                    } else {
                                        inicial.move_esp4(t0, t1, t2, t3, t4, t5, t6, t7);
                                    }
                                    return g2;
                                }

                            }
                        }
                    }
                }
                
            }
            
        }
    }
    return 0; 
}

void LinKerninghan::ejecutar(Solucion &inicial, const OneTree &ot,
                             const std::optional<Solucion> & mejor, std::mt19937 &engine) {
    
    int cont=0;
    while (cont++ < 8*inicial.ady.size()){
        int ganancia=search(inicial, ot, mejor,engine);
        if (ganancia>0){
            //assert(inicial.isConexa());
            inicial.costo-=ganancia; //actualiza el costo
            //assert(inicial.costo==inicial.evaluar(ot.mapa));
            cont=0;
        } 
    }
}
