#pragma once
#include "mapa.h"
#include <vector>

class OneTree {
    public:
      const Mapa mapa;
      OneTree(const Mapa mapa);
      std::vector<std::vector<int>> beta;
      int getAlfa(int i, int j) const;
      void generarCandidatos(const int MAXVEC);
      std::vector<std::vector<int>> candidatos;
};