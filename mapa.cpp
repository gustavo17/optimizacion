#include "mapa.h"
#include <fstream>
#include <iterator>
#include <queue>
#include <sstream>
#include <stdexcept>
#include <vector>
#include <iostream>

Mapa::Mapa(const std::string &filename) {
    // Abre el archivo en modo lectura
    std::ifstream file(filename);

    // Verifica si el archivo se abrió correctamente
    if (!file.is_open()) {
        throw std::runtime_error ("Error al abrir el archivo");
    }

    

    // Lee cada línea del archivo
    std::string line;
    while (getline(file, line)) {
        // Stream para dividir la línea en campos separados por comas
        std::stringstream ss{line};
        std::vector<int> row;
        std::string field;

        // Lee cada campo separado por comas y lo agrega al vector row
        while (getline(ss, field, ',')) {
            row.push_back(std::stol(field));
        }

        // Agrega la fila al vector de datos
        data.push_back(row);
    }

    // Cierra el archivo
    file.close();
}

//constructor de copia
Mapa::Mapa(const Mapa &map):data(map.data.size()) {
   //std::cout<<"COPIA DE MAP"<<std::endl;
    auto i=map.data.begin();
    for (auto j=data.begin();j<data.end();j++){
        std::copy(i->begin(),i->end(),std::back_inserter(*j));
        i++;
    }
}

Mapa::Mapa(const Mapa &&map):data{std::move(map.data)} {
   // std::cout<<"MOVIENDO MAP"<<std::endl;
}

int Mapa::prim(std::vector<int> & padres, const std::vector<int>*penalidades) const {
    using parPesoNodo=std::pair<int,int>;
    int ciudades=data.size();
    std::vector<bool> visited(ciudades,false);
    std::vector<int> menorDistancia(ciudades,std::numeric_limits<int>().max());
    std::priority_queue<parPesoNodo,std::vector<parPesoNodo>,std::greater<parPesoNodo>> pq;
    int largo=0;

    pq.push({0,1});
    
    while (!pq.empty()){
        int u=pq.top().second;
        if (!visited[u])largo+=pq.top().first;
        pq.pop(); 
        visited[u]=true;
        
        auto& datau=data[u];

        if (penalidades!=nullptr){
            int penu=(*penalidades)[u];
            for (int v=1;v<ciudades;v++) {
                if (v==u) continue;
                int weight=datau[v]+penu+(*penalidades)[v];
                if (!visited[v] && weight < menorDistancia[v] ){
                    menorDistancia[v]=weight;
                    pq.push({weight,v});
                    padres[v]=u;
                }
            }
        } else {
            for (int v=1;v<ciudades;v++) {
                if (v==u) continue;
                int weight=datau[v];
                if (!visited[v] && weight < menorDistancia[v] ){
                    menorDistancia[v]=weight;
                    pq.push({weight,v});
                    padres[v]=u;
                }
            }
        }
    }
    return largo;

}

int Mapa::getSize() const{
    return data.size();
}
