#include "oneTree.h" 
#include <functional>
#include <limits>
#include <queue>
#include <vector>


OneTree::OneTree(const Mapa m_mapa):mapa{std::move(m_mapa)},beta(mapa.data.size()),candidatos(mapa.data.size()) {
    for (int i=0;i<mapa.data.size();i++)
        beta[i].resize(mapa.data.size());
    std::vector<int> padres(mapa.data.size(),-1);
    mapa.prim(padres);
    std::vector<std::vector<int>> hijos(mapa.data.size());

    //crea vector de hijos por cada nodo
    for (int i=2;i<padres.size();i++){
        hijos[padres[i]].push_back(i);
    }

    std::vector<int> orden;
    std::function<void(int)> dfs=[&](int raiz){
        orden.push_back(raiz);
        for (auto nodo:hijos[raiz]){
            dfs(nodo);
        }
    };
    dfs(1);

    //comienza calculo de beta

    //seleccion de las 2 aristas mas cortas de 0
    int v1=std::numeric_limits<int>().max();
    int v2=v1;
    int i1=-1;
    int i2=-1;
    for (int i=1;i<mapa.data.size();i++){
        if (mapa.data[0][i]<v1){
            v1=mapa.data[0][i];
            i1=i;
        }
        if (i!=i1 && mapa.data[0][i]<v2){
            i2=i;
            v2=mapa.data[0][i];
        }
    }

    beta[0][0]=std::numeric_limits<int>().min();
    for (int i=1;i<mapa.data.size();i++){
        if (i==i1) {
            beta[0][i]=beta[i][0]=v1;
        } else {
            beta[0][i]=beta[i][0]=v2;
        }
    }

    int maxn=mapa.data.size()-1; //orden tiene ciudades-cero
    for (int i=0;i<maxn;i++){
        beta[orden[i]][orden[i]]=std::numeric_limits<int>().min();
        for (int j=i+1;j<maxn;j++){
            beta[orden[i]][orden[j]]=beta[orden[j]][orden[i]]=std::max(beta[orden[i]][padres[orden[j]]],mapa.data[orden[j]][padres[orden[j]]]);
        }
    }
}

int OneTree::getAlfa(int i, int j) const {
    return mapa.data[i][j]-beta[i][j];
}

void OneTree::generarCandidatos(const int MAXVEC) {
    using par=std::pair<int,int>;
    int n=mapa.data.size();
    for (int i=0;i<n;i++){
        std::priority_queue<par,std::vector<par>,std::greater<par>> y1Candidates;        
        for (int j=0;j<n;j++){
            if (i==j) continue;
            int alfa=getAlfa(i,j);
            y1Candidates.push(par({alfa,j}));
        }
        //toma las MAXVEC primeras
        auto & candi=candidatos[i]=std::vector<int>(MAXVEC);
        for (int k=0;k<MAXVEC;k++){
            int candidato=y1Candidates.top().second;
            y1Candidates.pop(); 
            candi[k]=candidato;
        }
    }
}
