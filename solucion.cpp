#include "solucion.h"
#include <format>
#include <iostream>
#include <ostream>
#include <random>
#include <utility>
#include "mapa.h"

Solucion::Solucion(int n,const OneTree & ot):ady(n),seleccionNodo{0,n-1} {
    //genera funcion uniformemente aleatoria
    std::random_device seeder;
    const auto seed {seeder.entropy() ? seeder() : time(nullptr)};
    std::mt19937 engine { static_cast<std::mt19937::result_type>(seed)};
    std::uniform_int_distribution<int> dist(0,n-1);
    
    std::vector<bool> visitados (n,false);
    int inicial=dist(engine);
    int actual=inicial;
    int cont=0;
    while (cont++<n){
        ady[actual].id=actual;
        visitados[actual]=true;

        int siguiente=inicial;
        if (cont<n){
            std::vector<int> candidatos_disponibles;
            auto & candidatos=ot.candidatos[actual];
            for (auto c:candidatos){
                if (!visitados[c]) candidatos_disponibles.push_back(c);
            }
            if (candidatos_disponibles.size()>0){       //seleccion por candidatos
                std::uniform_int_distribution<int> dist2(0,candidatos_disponibles.size()-1);
                siguiente=candidatos_disponibles[dist2(engine)];
            } else {                                    //seleccion al azar
                int posible=dist(engine);
                while (visitados[posible]){
                    posible=(posible+1)%n;
                }
                siguiente=posible;
            } 
        }
        ady[actual].siguiente=&ady[siguiente];
        ady[siguiente].anterior=&ady[actual];
        actual=siguiente;
    }
    
    //actualiza posiciones
    Nodo *inicio=&ady[0];
    inicio->posicion=0;
    Nodo *ptr_actual=inicio->siguiente;
    int pos=1;
    while (ptr_actual!=inicio){
        ptr_actual->posicion=pos++;
        ptr_actual=ptr_actual->siguiente;
    }
}


void Solucion::visualizar(std::ostream &out,bool ccw) const {
    const Nodo *inicio=&ady[0];
    Nodo * actual=(ccw)?inicio->siguiente:inicio->anterior;
    out<<inicio->id<<", ";
    while (actual!=inicio){
        out<<actual->id<<", ";
        actual=(ccw)?actual->siguiente:actual->anterior;
    }
    out<<"0\n\n";


    // inicio=&ady[0];
    // actual=(ccw)?inicio->siguiente:inicio->anterior;
    // std::cout<<inicio->posicion<<", ";
    // while (actual!=inicio){
    //     std::cout<<actual->posicion<<", ";
    //     actual=(ccw)?actual->siguiente:actual->anterior;
    // }
    // std::cout << std::endl;    

    out<<"COSTO="<<costo<<"\n";
}


int Solucion::evaluar(const Mapa &m)  {
    Nodo *inicio=&ady[0];
    Nodo *actual=inicio->siguiente;
    int suma=m.data[inicio->id][actual->id];
    while (actual!=inicio){
        actual=actual->siguiente;
        suma+=m.data[actual->anterior->id][actual->id];
    }
    return suma;
}

Solucion::Solucion(const Solucion &sol):ady(sol.ady.size()),seleccionNodo{0,static_cast<int>(sol.ady.size())-1} {
    //std::cout<<"copia"<<std::endl;
    for (int i=0;i<ady.size();i++){
        ady[i].id=sol.ady[i].id;
        ady[i].posicion=sol.ady[i].posicion;
        ady[i].siguiente=&ady[sol.ady[i].siguiente->id];
        ady[i].anterior=&ady[sol.ady[i].anterior->id];
    }
    costo=sol.costo;
}

Solucion::Solucion(Solucion &&sol):ady{std::move(sol.ady)},costo{sol.costo},seleccionNodo{std::move(sol.seleccionNodo)} {
    //std::cout<<"movimiento"<<std::endl;
}

Solucion &Solucion::operator=(Solucion &&sol) {
    //std::cout<<"asignacion de movimiento"<<std::endl;
    ady=std::move(sol.ady);
    costo=sol.costo;
    seleccionNodo=std::move(sol.seleccionNodo);
    return *this;
}

bool Solucion::isConexa() const {
    //es mas eficiente que bool (por empaquetado)
    const Nodo *inicio=&ady[0];
    Nodo * actual=inicio->siguiente;
    int cont=1;
    int pos=inicio->posicion;
    pos++;if (pos==ady.size())pos=0;
    while (actual!=inicio){
        if (pos!=actual->posicion) {
            std::cout<<"esperada : "<<pos<<", encontrada :"<<actual->posicion<< ", en nodo "<<actual->id<<std::endl;
            return false;
        }
        pos++;if (pos==ady.size())pos=0;
        cont++;
        if (actual->siguiente->anterior!=actual){
            std::cout<<"anterior mal configurado en nodo: "<<actual->siguiente->id<<std::endl;
            return false;
        }
        actual=actual->siguiente;
        if (cont>ady.size()) return false;
    }
    return cont==ady.size();
}


Solucion::Solucion(std::vector<int> ruta):ady(ruta.size()-1),seleccionNodo{0,static_cast<int>(ruta.size())-1} {
    for (int i=1;i<ruta.size();i++){
        int a=ruta[i];
        int b=ruta[i-1];
        Nodo * actual=&ady[a];
        Nodo * anterior=&ady[b];
        anterior->siguiente=actual;
        actual->anterior=anterior;
        actual->id=a;
        actual->posicion=i;
    }
    ady[0].id=0;
    ady[0].posicion=0;
}

int Solucion::getSize() const {
    return ady.size();
}

//vamos a simplificar siempre t1 va a ser t0 siguiente
void Solucion::move(Nodo *t0, Nodo *t1, Nodo *t2, Nodo *t3) {
    int n=ady.size();

    //calculo de elementos
    int cant1=t3->posicion-t1->posicion;
    if (cant1<0) cant1+=n;

    int cant2=t0->posicion-t2->posicion;
    if (cant2<0)cant2+=n;


    if (t0->siguiente==t1) {
        if (cant1<=cant2){
            _move(t0,t1,t2,t3);
        } else {
            _move(t3,t2,t1,t0);
        }
    } else {
        if (cant1<=cant2){
            _move(t1,t0,t3,t2);
        } else {
            _move(t2,t3,t0,t1);
        }
    }
}

void Solucion::_move(Nodo *t0, Nodo *t1, Nodo *t2, Nodo *t3) {
    int n=ady.size();
    Nodo * ptr=t1;
    int pos=t3->posicion;
    while (ptr!=t2){
        ptr->posicion=pos--; if (pos<0)pos=n-1;
        std::swap(ptr->siguiente,ptr->anterior);
        ptr=ptr->anterior;
    }
    t0->siguiente=t3;t3->anterior=t0;
    t1->siguiente=t2;t2->anterior=t1;
}

//t4->siguiente=t5 y t4 ENTRE t1 y t2
void Solucion::_move_esp1(Nodo *t0, Nodo *t1, Nodo *t2, Nodo *t3,Nodo *t4,Nodo *t5) {
    int n=ady.size();
    Nodo *ptr=t5;
    int pos=t0->posicion+1;if (pos==n)pos=0;
    while (ptr!=t3){
        ptr->posicion=pos++;if (pos==n)pos=0;
        ptr=ptr->siguiente;
    }
    ptr=t1;
    while (ptr!=t5){
        ptr->posicion=pos++;if (pos==n)pos=0;
        ptr=ptr->siguiente;
    }
    t0->siguiente=t5;t5->anterior=t0;
    t2->siguiente=t1;t1->anterior=t2;
    t4->siguiente=t3;t3->anterior=t4;
}
void Solucion::move_esp1(Nodo *t0, Nodo *t1, Nodo *t2, Nodo *t3, Nodo *t4,Nodo *t5) {
    int n=ady.size();
    int t0t3=t0->posicion-t3->posicion; if (t0t3<0)t0t3+=n;
    int t2t5=t2->posicion-t5->posicion; if (t2t5<0)t2t5+=n;
    int t4t1=t4->posicion-t1->posicion; if (t4t1<0)t4t1+=n;
    
    //elige el movimiento optimo (de menor tiempo)
    if (t0t3>t2t5){
        if (t4t1>t0t3) {
            _move_esp1(t4, t5, t0, t1, t2, t3);
        } else {
            _move_esp1(t0, t1, t2, t3, t4, t5);
        }
    } else {
        if (t4t1>t2t5){
            _move_esp1(t4, t5, t0, t1, t2, t3);
        } else {
            _move_esp1(t2, t3, t4, t5, t0, t1);
        }
    }
}

void Solucion::move_esp2(Nodo *t0, Nodo *t1, Nodo *t2, Nodo *t3, Nodo *t4, Nodo *t5) {
    int n=ady.size();
    int t0t3=t0->posicion-t3->posicion; if (t0t3<0)t0t3+=n;
    int t2t4=t2->posicion-t4->posicion; if (t2t4<0)t2t4+=n;
    int t5t1=t5->posicion-t1->posicion; if (t5t1<0)t5t1+=n;
    
    //elige el movimiento optimo (de menor tiempo)
    if (t0t3>t2t4){
        if (t5t1>t0t3) {
            _move_esp2_2(t0, t1, t2, t3, t4, t5);
        } else {
            _move_esp2_0(t0, t1, t2, t3, t4, t5);
        }
    } else {
        if (t5t1>t2t4){
            _move_esp2_2(t0, t1, t2, t3, t4, t5);
        } else {
            _move_esp2_1(t0, t1, t2, t3, t4, t5);
        }
    }
}
void Solucion::_move_esp2_0(Nodo *t0, Nodo *t1, Nodo *t2, Nodo *t3, Nodo *t4, Nodo *t5) {
    int n=ady.size();
    Nodo *ptr=t5;
    int pos=t0->posicion+1;if (pos==n)pos=0;
    while (ptr!=t0){
        ptr->posicion=pos++;if (pos==n)pos=0;
        std::swap(ptr->siguiente,ptr->anterior);
        ptr=ptr->siguiente;
    }
    ptr=t2;
    while (ptr!=t5){
        ptr->posicion=pos++;if (pos==n)pos=0;
        std::swap(ptr->siguiente,ptr->anterior);
        ptr=ptr->siguiente;
    }
    t0->siguiente=t5;t5->anterior=t0;
    t1->siguiente=t2;t2->anterior=t1;
    t4->siguiente=t3;t3->anterior=t4;    
}
void Solucion::_move_esp2_1(Nodo *t0, Nodo *t1, Nodo *t2, Nodo *t3, Nodo *t4, Nodo *t5) {
    int n=ady.size();
    Nodo *ptr=t1;
    int pos=t2->posicion+1;if (pos==n)pos=0;
    while (ptr!=t4){
        ptr->posicion=pos++;if (pos==n)pos=0;
        ptr=ptr->siguiente;
    }
    ptr=t0;
    while (ptr!=t2){
        ptr->posicion=pos++;if (pos==n)pos=0;
        std::swap(ptr->siguiente,ptr->anterior);
        ptr=ptr->siguiente;
    }
    t2->siguiente=t1;t1->anterior=t2;
    t5->siguiente=t0;t0->anterior=t5;
    t3->siguiente=t4;t4->anterior=t3;
}
void Solucion::_move_esp2_2(Nodo *t0, Nodo *t1, Nodo *t2, Nodo *t3, Nodo *t4, Nodo *t5) {
    int n=ady.size();
    Nodo *ptr=t0;
    int pos=t5->posicion+1;if (pos==n)pos=0;
    while (ptr!=t2){
        ptr->posicion=pos++;if (pos==n)pos=0;
        std::swap(ptr->siguiente,ptr->anterior);
        ptr=ptr->siguiente;
    }
    ptr=t4;
    while (ptr!=t3  ){
        ptr->posicion=pos++;if (pos==n)pos=0;
        ptr=ptr->siguiente;
    }
    t2->siguiente=t1;t1->anterior=t2;
    t5->siguiente=t0;t0->anterior=t5;
    t3->siguiente=t4;t4->anterior=t3;
}
void Solucion::move_noseq(Nodo *t0, Nodo *t1, Nodo *t2, Nodo *t3, Nodo *_t0,
                          Nodo *_t1, Nodo *_t2, Nodo *_t3) {

    int n = ady.size();
    
    //Movimiento No Factible de Inicio

    if (_t2->anterior==_t3){
        int a2=t0->posicion-t3->posicion;if (a2<0)a2+=n;
        int b2=t2->posicion-t1->posicion;if (b2<0)b2+=n;

        Nodo *inicio2,*fin2;
        if (a2>b2){
            //invierte
            inicio2=t1;fin2=_t2;

            int pos=_t3->posicion;
            while (inicio2!=fin2) {
                inicio2->posicion=pos--;if (pos<0)pos=n-1;
                std::swap(inicio2->siguiente,inicio2->anterior);
                inicio2=inicio2->anterior;
            }
            inicio2=_t2;fin2=t3;
            
            pos=t2->posicion;
            while (inicio2!=fin2) {
                inicio2->posicion=pos--;if (pos<0)pos=n-1;
                std::swap(inicio2->siguiente,inicio2->anterior);
                inicio2=inicio2->anterior;
            }

            t0->siguiente=t3;t3->anterior=t0;
            _t2->siguiente=_t1;_t1->anterior=_t2;   
            t1->siguiente=t2;t2->anterior=t1;
            _t0->siguiente=_t3;_t3->anterior=_t0;

            int a=_t0->posicion-t3->posicion;if (a<0)a+=n;
            int b=t0->posicion-_t1->posicion;if (b<0)b+=n;
            int c=t1->posicion-_t3->posicion;if (c<0)c+=n;
            int d=_t2->posicion-t2->posicion;if (d<0)d+=n;
            
            // selecciona el mejor inicio
            Nodo * inicio=_t0,* fin=t3; 
            int mayor=a;
            if (b>a) {
                inicio=t0; fin=_t1;
                mayor=b;
            }
            if (c>mayor){
                inicio=t1; fin=_t3;
                mayor=c;
            } 
            if (d>mayor){
                inicio=_t2; fin=t2;
            } 

            pos=inicio->posicion+1;if (pos==n)pos=0;
            inicio=inicio->siguiente;    
            while (inicio!=fin) {
                inicio->posicion=pos++;if (pos==n)pos=0;
                inicio=inicio->siguiente;
            }

        } else {       
            //invierte
            int pos=_t0->posicion;
            inicio2=t3;
            
            while (inicio2!=_t1) {
                inicio2->posicion=pos--;if (pos<0)pos=n-1;
                std::swap(inicio2->siguiente,inicio2->anterior);
                inicio2=inicio2->anterior;
            }
            pos=t0->posicion;
            inicio2=_t1;
            
            while (inicio2!=t1) {
                inicio2->posicion=pos--;if (pos<0)pos=n-1;
                std::swap(inicio2->siguiente,inicio2->anterior);
                inicio2=inicio2->anterior;
            }


            t3->siguiente=t0;t0->anterior=t3;
            _t1->siguiente=_t2;_t2->anterior=_t1;   
            t2->siguiente=t1;t1->anterior=t2;
            _t3->siguiente=_t0;_t0->anterior=_t3;

            int a=t3->posicion-_t0->posicion;if (a<0)a+=n;
            int b=_t1->posicion-t0->posicion;if (b<0)b+=n;
            int c=_t3->posicion-t1->posicion;if (c<0)c+=n;
            int d=t2->posicion-_t2->posicion;if (d<0)d+=n;
            
            // selecciona el mejor inicio
            Nodo * inicio=t3,* fin=_t0; 
            int mayor=a;      
            if (b>a) {      
                inicio=_t1; fin=_t0;
                mayor=b;
            }
            if (c>mayor){
                inicio=_t3; fin=t1;
                mayor=c;
            } 
            if (d>mayor){                
                inicio=t2; fin=_t2;
            } 

            pos=inicio->posicion+1;if (pos==n)pos=0;
            inicio=inicio->siguiente;    
            while (inicio!=fin) {
                inicio->posicion=pos++;if (pos==n)pos=0;
                inicio=inicio->siguiente;
            }

        }

    } else {
        t0->siguiente=t3;t3->anterior=t0;
        t1->anterior=t2;t2->siguiente=t1;
        _t0->siguiente=_t3;_t3->anterior=_t0;    
        _t2->siguiente=_t1;_t1->anterior=_t2; 
        
        int a=_t0->posicion-t3->posicion;if (a<0)a+=n;
        int b=t0->posicion-_t1->posicion;if (b<0)b+=n;
        int c=_t2->posicion-t1->posicion;if (c<0)c+=n;
        int d=t2->posicion-_t3->posicion;if (d<0)d+=n;
        
        // selecciona el mejor inicio
        Nodo * inicio=_t0,* fin=t3;
        int mayor=a;
        if (b>a) {
            inicio=t0; fin=_t1;
            mayor=b;
        }
        if (c>mayor){
            inicio=_t2; fin=t1;
            mayor=c;
        } 
        if (d>mayor){
            inicio=t2; fin=_t3;
        } 

        int pos=inicio->posicion+1;if (pos==n)pos=0;
        inicio=inicio->siguiente;    
        while (inicio!=fin) {
            inicio->posicion=pos++;if (pos==n)pos=0;
            inicio=inicio->siguiente;
        }

    }
}

//podria mejorarse si para casos de inversion grande, entonces se inviertan las otras 3
//para t7 siguiente
void Solucion::move_esp3(Nodo *t0, Nodo *t1, Nodo *t2, Nodo *t3, Nodo *t4,
                         Nodo *t5, Nodo *t6, Nodo *t7) {

    int n=ady.size();
    int pos=t5->posicion;
    Nodo *ptr=t3;
    while (ptr!=t4){
        ptr->posicion=pos--;if (pos<0)pos=n-1;
        std::swap(ptr->siguiente,ptr->anterior);
        ptr=ptr->anterior;
    }
    t0->siguiente=t7;t7->anterior=t0;
    t2->siguiente=t1;t1->anterior=t2;
    t6->siguiente=t5;t5->anterior=t6;
    t3->siguiente=t4;t4->anterior=t3;

    //reposicionamiento
    int a=t3->posicion-t5->posicion;if (a<0)a+=n;
    int b=t0->posicion-t4->posicion;if (b<0)b+=n;
    int c=t6->posicion-t1->posicion;if (c<0)c+=n;
    int d=t2->posicion-t7->posicion;if (d<0)d+=n;
    
    // selecciona el mejor inicio
    Nodo * inicio=t0,* fin=t5; //inicio es t3, pero segm b es correlativo, por eso sale desde t0
    int mayor=a;
    if (b>a) {
        inicio=t0; fin=t4;
        mayor=b;
    }
    if (c>mayor){
        inicio=t6; fin=t1;
        mayor=c;
    } 
    if (d>mayor){
        inicio=t2; fin=t7;
    } 

    pos=inicio->posicion+1;if (pos==n)pos=0;
    inicio=inicio->siguiente;    
    while (inicio!=fin) {
        inicio->posicion=pos++;if (pos==n)pos=0;
        inicio=inicio->siguiente;
    }
}
//para t7 anterior
void Solucion::move_esp4(Nodo *t0, Nodo *t1, Nodo *t2, Nodo *t3, Nodo *t4,
                         Nodo *t5, Nodo *t6, Nodo *t7) {


    int n=ady.size();
    int pos=t0->posicion;
    Nodo *ptr=t4;
    while (ptr!=t1){
        ptr->posicion=pos--;if (pos<0)pos=n-1;
        std::swap(ptr->siguiente,ptr->anterior);
        ptr=ptr->anterior;
    }
    t7->siguiente=t0;t0->anterior=t7;
    t2->siguiente=t1;t1->anterior=t2;
    t5->siguiente=t6;t6->anterior=t5;
    t4->siguiente=t3;t3->anterior=t4;

    //reposicionamiento
    int a=t5->posicion-t3->posicion;if (a<0)a+=n;
    int b=t4->posicion-t0->posicion;if (b<0)b+=n;
    int c=t7->posicion-t1->posicion;if (c<0)c+=n;
    int d=t2->posicion-t6->posicion;if (d<0)d+=n;
    
    // selecciona el mejor inicio
    Nodo * inicio=t5,* fin=t3;
    int mayor=a;
    if (b>a) {
        inicio=t4; fin=t0;
        mayor=b;
    }
    if (c>mayor){
        inicio=t7; fin=t1;
        mayor=c;
    } 
    if (d>mayor){
        inicio=t2; fin=t6;
    } 

    pos=inicio->posicion+1;if (pos==n)pos=0;
    inicio=inicio->siguiente;    
    while (inicio!=fin) {
        inicio->posicion=pos++;if (pos==n)pos=0;
        inicio=inicio->siguiente;
    }

}
