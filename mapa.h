#pragma once
#include <cstddef>
#include <string>
#include <vector>

class Mapa {
    public:
        Mapa()=default;
        Mapa(const std::string &filename);
        Mapa(const Mapa &map);
        Mapa(const Mapa &&map);
        int prim(std::vector<int> & padres, const std::vector<int> *penalidades=nullptr) const;
        std::vector<std::vector<int>> data;
        int getSize() const;
};